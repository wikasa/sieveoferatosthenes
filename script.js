//An algorithm to find the nth prime number - sieve of Eratosthenes
//A prime number is a natural number that has exactly two distinct natural number divisors: the number 1 and itself. 
//To find all the prime numbers less than or equal to a given integer n by Eratosthenes' method:
//  1. Create a list of consecutive integers from 2 through n: (2, 3, 4, ..., n).
//  2. Initially, let p equal 2, the smallest prime number.
//  3. Enumerate the multiples of p by counting in increments of p from 2p to n, and mark them in the list (these will be 2p, 3p, 4p, ...; the p itself should not be marked).
//  4. Find the smallest number in the list greater than p that is not marked. If there was no such number, stop. Otherwise, let p now equal this new number (which is the next prime), and repeat from step 3.
//  5. When the algorithm terminates, the numbers remaining not marked in the list are all the primes below n.
//As a refinement, it is sufficient to mark the numbers in step 3 starting from p^2, as all the smaller multiples of p will have already been marked at that point. This means that the algorithm is allowed to terminate in step 4 when p^2 is greater than n.
const {performance} = require('perf_hooks');
const n = 30
const sieve = []
for(let i = 2;i<=n;i++){
    sieve[i-2] = i
}
function sieveOfEratosthenesBasic(n, sieve){
    for(let p = 2;p<n;p++){
        for(let i = 2;(i*p)<=n;i++){
            sieve[i*p-2] = false
        }
    let result = sieve.filter(Boolean)
    return result
    }
}
function sieveOfEratosthenes(n, sieve){
    for(let p = 2;p<n;p++){
        if(!sieve[p-2]) continue //optimized, algorithm do not try every number
        for(let i = 2;(i*p)<=n;i++){
            sieve[i*p-2] = false
        }
    }
    let result = sieve.filter(Boolean)
    return result
}
function sieveOfEratosthenesOptimized(n, sieve){
    for(let p = 2;p<n;p++){
        if(p*p<=n){
            for(let i = 2;(i*p)<=n;i++){
                sieve[i*p-2] = false
            }
        }
    }
    let result = sieve.filter(Boolean)
    return result
}
//This algorithm produces all primes not greater than n. It includes a common optimization, which is to start enumerating the multiples of each prime i from i2.
function sieveOfEratosthenesConversely(n){
    const isPrime = new Array(n+1).fill(true)
    isPrime[0] = false
    isPrime[1] = false
    const sieve = []
    for(let p = 2;p<=n;p++){
        if(isPrime[p]===true){
            sieve.push(p)
            let next = p*p
            while(next<=n){
                isPrime[next]=false
                next+=p
            }
        }
    }
    return sieve
}
let t0 = performance.now()
console.log(sieveOfEratosthenesBasic(n,sieve))
//console.log(sieveOfEratosthenes(n,sieve))
//console.log(sieveOfEratosthenesOptimized(n,sieve)) // I'm the fastest!
//console.log(sieveOfEratosthenesConversely(n))
let t1 = performance.now()
console.log("This version of sieve is done in " + (t1 - t0) + " milliseconds.")